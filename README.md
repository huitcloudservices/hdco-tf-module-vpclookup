# Terraform Module for VPC and Subnet Lookups

This module provide a Terraform data interface to enable VPC and Subnet Lookups where the they do not exist in the state file

## Module Invocation/Usage

```
module "vpc_lookup" {
  source                = "bitbucket.org/huitcloudservices/hcdo-tf-module-vpclookup"
  group_name            = "mygroup"
  environment           = "dev"
  # Optional (disabled)
  // vpc_name = "my_nonstandard_naming"
}
```

## Inputs

| Name | Description | Default | Required |
|------|-------------|:-----:|:-----:|
| group_name | Name of group used in the naming standard | - | yes |
| environment | Environment (dev,prod) for VPC | - | yes |
| account_type | Type of account (standard, level4) | `"standard"` | no |
| az_count | Number of AZ's provisioned | `"3"` | no |
| vpc_name | Override for non-common named VPCs | - | no |


## Outputs

| Name | Description |
|------|-------------|
| vpc_id | VPC ID of the discovered VPC |
| elb_private_standard_ids | List of ID's for the elb private standard subnets |
| elb_private_level4_ids | List of ID's for the elb private level4 subnets |
| app_private_standard_ids | List of ID's for the app private standard subnets |
| app_private_level4_ids | List of ID's for the app private level4 subnets |
| db_private_standard_ids | List of ID's for the DB private standard subnets |
| db_private_level4_ids | List of ID's for the DB private level4 subnets |
| elb_private_standard | Map of ID to AZ, Name |
| elb_private_level4 | Map of ID to AZ, Name |
| app_private_standard | Map of ID to AZ, Name |
| app_private_level4 | Map of ID to AZ, Name |
| db_private_standard | Map of ID to AZ, Name |
| db_private_level4 | Map of ID to AZ, Name |

