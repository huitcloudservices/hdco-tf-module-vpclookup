variable "group_name" {
  type = "string"
  description = "Name of group used in the naming standard"
}
variable "vpc_name" {
  type = "string"
  default = ""
  description = "Override for non-common named VPCs"
}

variable "environment" {
  type = "string"
  description = "Environment (dev,prod) for VPC"
}
variable "account_type" {
  type = "string"
  default = "standard"
  description = "Type of account (standard, level4)"
}

variable "az_count" {
  type = "string"
  default = "3"
  description = "Number of AZ's provisioned"
}