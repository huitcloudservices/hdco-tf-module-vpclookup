output "vpc_id" {value = "${data.aws_vpc.vpc.id}"}
output "elb_private_standard_ids" { value = ["${data.aws_subnet.elb_private_standard.*.id}"]}
output "elb_private_level4_ids" { value = ["${data.aws_subnet.elb_private_level4.*.id}"]}
output "app_private_standard_ids" { value = ["${data.aws_subnet.app_private_standard.*.id}"]}
output "app_private_level4_ids" { value = ["${data.aws_subnet.app_private_level4.*.id}"]}
output "db_private_standard_ids" { value = ["${data.aws_subnet.db_private_standard.*.id}"]}
output "db_private_level4_ids" { value = ["${data.aws_subnet.db_private_level4.*.id}"]}

output "elb_private_standard" { value =
  {
    "${element(data.aws_subnet.elb_private_standard.*.id, 0)}" = {
      az = "${element(data.aws_subnet.elb_private_standard.*.availability_zone, 0)}"
      name = "${element(data.aws_subnet.elb_private_standard.*.tags.Name, 0)}"
    },
    "${element(data.aws_subnet.elb_private_standard.*.id, 1)}" = {
      az = "${element(data.aws_subnet.elb_private_standard.*.availability_zone, 1)}"
      name = "${element(data.aws_subnet.elb_private_standard.*.tags.Name, 1)}"
    },
     "${element(data.aws_subnet.elb_private_standard.*.id, 2)}" = {
      az = "${element(data.aws_subnet.elb_private_standard.*.availability_zone, 2)}"
      name = "${element(data.aws_subnet.elb_private_standard.*.tags.Name, 2)}"
    }
  }
}


output "elb_private_level4" { value =
  {
    "${element(data.aws_subnet.elb_private_level4.*.id, 0)}" = {
      az = "${element(data.aws_subnet.elb_private_level4.*.availability_zone, 0)}"
      name = "${element(data.aws_subnet.elb_private_level4.*.tags.Name, 0)}"
    },
    "${element(data.aws_subnet.elb_private_level4.*.id, 1)}" = {
      az = "${element(data.aws_subnet.elb_private_level4.*.availability_zone, 1)}"
      name = "${element(data.aws_subnet.elb_private_level4.*.tags.Name, 1)}"
    },
     "${element(data.aws_subnet.elb_private_level4.*.id, 2)}" = {
      az = "${element(data.aws_subnet.elb_private_level4.*.availability_zone, 2)}"
      name = "${element(data.aws_subnet.elb_private_level4.*.tags.Name, 2)}"
    }
  }
}

output "app_private_standard" { value =
  {
    "${element(data.aws_subnet.app_private_standard.*.id, 0)}" = {
      az = "${element(data.aws_subnet.app_private_standard.*.availability_zone, 0)}"
      name = "${element(data.aws_subnet.app_private_standard.*.tags.Name, 0)}"
    },
    "${element(data.aws_subnet.app_private_standard.*.id, 1)}" = {
      az = "${element(data.aws_subnet.app_private_standard.*.availability_zone, 1)}"
      name = "${element(data.aws_subnet.app_private_standard.*.tags.Name, 1)}"
    },
     "${element(data.aws_subnet.app_private_standard.*.id, 2)}" = {
      az = "${element(data.aws_subnet.app_private_standard.*.availability_zone, 2)}"
      name = "${element(data.aws_subnet.app_private_standard.*.tags.Name, 2)}"
    }
  }
}

output "app_private_level4" { value =
  {
    "${element(data.aws_subnet.app_private_level4.*.id, 0)}" = {
      az = "${element(data.aws_subnet.app_private_level4.*.availability_zone, 0)}"
      name = "${element(data.aws_subnet.app_private_level4.*.tags.Name, 0)}"
    },
    "${element(data.aws_subnet.app_private_level4.*.id, 1)}" = {
      az = "${element(data.aws_subnet.app_private_level4.*.availability_zone, 1)}"
      name = "${element(data.aws_subnet.app_private_level4.*.tags.Name, 1)}"
    },
     "${element(data.aws_subnet.app_private_level4.*.id, 2)}" = {
      az = "${element(data.aws_subnet.app_private_level4.*.availability_zone, 2)}"
      name = "${element(data.aws_subnet.app_private_level4.*.tags.Name, 2)}"
    }
  }
}

output "db_private_standard" { value =
  {
    "${element(data.aws_subnet.db_private_standard.*.id, 0)}" = {
      az = "${element(data.aws_subnet.db_private_standard.*.availability_zone, 0)}"
      name = "${element(data.aws_subnet.db_private_standard.*.tags.Name, 0)}"
    },
    "${element(data.aws_subnet.db_private_standard.*.id, 1)}" = {
      az = "${element(data.aws_subnet.db_private_standard.*.availability_zone, 1)}"
      name = "${element(data.aws_subnet.db_private_standard.*.tags.Name, 1)}"
    },
     "${element(data.aws_subnet.db_private_standard.*.id, 2)}" = {
      az = "${element(data.aws_subnet.db_private_standard.*.availability_zone, 2)}"
      name = "${element(data.aws_subnet.db_private_standard.*.tags.Name, 2)}"
    }
  }
}

output "db_private_level4" { value =
  {
    "${element(data.aws_subnet.db_private_level4.*.id, 0)}" = {
      az = "${element(data.aws_subnet.db_private_level4.*.availability_zone, 0)}"
      name = "${element(data.aws_subnet.db_private_level4.*.tags.Name, 0)}"
    },
    "${element(data.aws_subnet.db_private_level4.*.id, 1)}" = {
      az = "${element(data.aws_subnet.db_private_level4.*.availability_zone, 1)}"
      name = "${element(data.aws_subnet.db_private_level4.*.tags.Name, 1)}"
    },
     "${element(data.aws_subnet.db_private_level4.*.id, 2)}" = {
      az = "${element(data.aws_subnet.db_private_level4.*.availability_zone, 2)}"
      name = "${element(data.aws_subnet.db_private_level4.*.tags.Name, 2)}"
    }
  }
}