data "aws_vpc" "vpc" {
  filter {
    name = "tag:Name"
    values = ["${coalesce(var.vpc_name, "${var.group_name}-${var.environment}-${var.account_type}")}"]
  }
}

//output "azs" {value = "${element(split("-", element(data.aws_availability_zones.aws_azs.names, 0)),2)}"}


data "aws_subnet" "elb_private_standard" {
  vpc_id = "${data.aws_vpc.vpc.id}"
  count = "${var.az_count}"
  availability_zone = "${element(data.aws_availability_zones.aws_azs.names, count.index)}"
   filter {
     name = "tag:Name"
     values = ["${var.group_name}-standard-elb-private-${element(split("-", element(data.aws_availability_zones.aws_azs.names, count.index)), 2)}-1"]
   }
}

data "aws_subnet" "elb_private_level4" {
  vpc_id = "${data.aws_vpc.vpc.id}"
  count = "${var.az_count}"
  availability_zone = "${element(data.aws_availability_zones.aws_azs.names, count.index)}"
   filter {
     name = "tag:Name"
     values = ["${var.group_name}-level4-elb-private-${element(split("-", element(data.aws_availability_zones.aws_azs.names, count.index)), 2)}-1"]
   }
}

data "aws_subnet" "app_private_standard" {
  vpc_id = "${data.aws_vpc.vpc.id}"
  count = "${var.az_count}"
  availability_zone = "${element(data.aws_availability_zones.aws_azs.names, count.index)}"
    filter {
      name = "tag:Name"
      values = ["${var.group_name}-standard-app-private-${element(split("-", element(data.aws_availability_zones.aws_azs.names, count.index)), 2)}-1"]
    }
}

data "aws_subnet" "app_private_level4" {
  vpc_id = "${data.aws_vpc.vpc.id}"
  count = "${var.az_count}"
  availability_zone = "${element(data.aws_availability_zones.aws_azs.names, count.index)}"
  filter {
    name = "tag:Name"
    values = ["${var.group_name}-level4-app-private-${element(split("-", element(data.aws_availability_zones.aws_azs.names, count.index)), 2)}-1"]
  }
}

data "aws_subnet" "db_private_standard" {
  vpc_id = "${data.aws_vpc.vpc.id}"
  count = "${var.az_count}"
  availability_zone = "${element(data.aws_availability_zones.aws_azs.names, count.index)}"
  filter {
    name = "tag:Name"
    values = ["${var.group_name}-standard-db-private-${element(split("-", element(data.aws_availability_zones.aws_azs.names, count.index)), 2)}-1"]
  }
}

data "aws_subnet" "db_private_level4" {
  vpc_id = "${data.aws_vpc.vpc.id}"
  count = "${var.az_count}"
  availability_zone = "${element(data.aws_availability_zones.aws_azs.names, count.index)}"
  filter {
    name = "tag:Name"
    values = ["${var.group_name}-level4-db-private-${element(split("-", element(data.aws_availability_zones.aws_azs.names, count.index)), 2)}-1"]
  }
}
