# This file should be used in the event the criteria mentioned in main.tf is met
data "aws_availability_zones" "aws_azs" {
  state = "available"
}